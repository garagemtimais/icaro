# -*- coding: UTF-8 -*-
import os 
import time 
import socket 
import shlex 
import subprocess 
import commands 
import re 
from random import randint
from slackclient import SlackClient 
import sys 
import urllib

reload(sys) 
sys.setdefaultencoding('utf-8')

# starterbot's ID as an environment variable
BOT_ID = "D441J68HE"

# constants
AT_BOT = "icaro"
EXAMPLE_COMMAND = "do"

# instantiate Slack & Twilio clients
slack_client = SlackClient('xoxb-141450244727-07q16m0ZIpm1aIUGBQQg8gP7') 

def handle_command(command, channel, userid):
    username = ""
    image = ""
    api_call = slack_client.api_call("users.info", user=userid)
    print command
    if api_call.get('user'):
        username = api_call.get('user').get("name")

    if username == "icaro":
        return

    que = ["bem loko","que tem eu?", "empolgante", "fala na cara mano", "começou a enxer meu saco já", "que?", "sei não", "irineu, você não sabe nem eu"]
    response = que[randint(0,len(que)-1)]

    import json
    file = open('dados.json', 'r')
    info = json.load(file)

   
    if command.startswith("o que é "):
        if command.endswith("?"):
            command = command[:len(command)-1]
        sch = command[8:]
        response = "https://pt.wikipedia.org/wiki/" + sch

    elif command.startswith("quem foi "):
        if command.endswith("?"):
            command = command[:len(command)-1]

        sch = command[9:]
        response = "https://pt.wikipedia.org/wiki/" + sch


    elif command.startswith("quem é "):
        if command.endswith("?"):
            command = command[:len(command)-1]
        sch = command[7:]
        response = "https://pt.wikipedia.org/wiki/" + sch


    elif command.startswith("o que significa "):
        if command.endswith("?"):
            command = command[:len(command)-1]
        sch = command[16:]
        response = "https://pt.wikipedia.org/wiki/" + sch

    elif command.startswith("onde "):
        if command.endswith("?"):
            command = command[:len(command)-1]
        command = command[5:]
	sch = command
        if command.startswith("é "):
            sch = command[2:]
            if command.startswith("é o ") or command.startswith("é a "):
                sch = command[4:]

        if command.startswith("fica o ") or command.startswith("fica a "):
            sch = command[7:]
        
        response = sch
	sch = urllib.quote(sch)
	
        image = {"title":"https://www.google.com.br/maps/?q="+ sch +"/", "image_url" : "https://maps.googleapis.com/maps/api/staticmap?center="+sch+",BR&zoom=15&size=600x300&key=AIzaSyCLxZq6_YVfmFKKqFpZ5g4JNQPAqpVw3YQ"}

    elif "quem sou" in command:
        response = username

    elif "quando eu disser " in command and "diga" in command:
        apren = command[17:]
        print apren
        aprend = apren.split("diga")
        talk = aprend[0].strip()
        res = aprend[1].strip()
        print "%s => %s " % (talk, res)

        if info.has_key(talk):
            info[talk].append(res)
        else:
            info[talk] = [res]

        file = open('dados.json', 'w+')
        json.dump(info, file)
        file.close()
        response = "ok"
        

    elif "birrl" in command:
        que = ["aqui é bodybuilder poha", "ta saindo da jaula o monstro", "aqui nois constroi fibra", "hora do show poha", "não é agua com músculo", "eita poha, ta saindo", "vem monstro"]
        response = que[randint(0,len(que)-1)]    

    elif "mata" in command:
        que = ["aqui tem corage", ":mata:", "aqui meu amigo, não tremeu não"]
        response = que[randint(0,len(que)-1)]

    elif "é uai" in command:
        que = ["o que tem que se fazer.. tem que se fazer com as pessoa que se merece isso", "nada a ver irmão", "desruga to miha pele sendo que eu não mereço isso", "tudo errado essas pergunta", "vida pro resto da sua vida"]
        response = que[randint(0,len(que)-1)]

    elif "você acha d" in command or "vc acha d" in command:
        que = ["vish", "legal", "preciso de auto-conciencia pra ter opinião", "pergunta lá no posto ipiranga", "irineu.. você não sabe nem eu", ""]
        response = que[randint(0,len(que)-1)]

    elif "ta quanto" in command or "to custa" in command:
        que = [" "]
        image = {"title": "custa quinhento conto", "image_url": "http://imgur.com/download/3z2hApe"}
        response = que[randint(0,len(que)-1)]

    elif "burro" in command:
        que = ["sua mãe ta boa?", "vai te fude", "toma no cu carai", "ta sem trampo seu puto?", "falta do que fazer é foda"]
        response = que[randint(0,len(que)-1)]


    elif command.startswith('&lt;') and command.endswith('&gt;'):
        comando = command[4:-4]
        p = commands.getoutput(comando)
            
        response = "```%s```" % (p)

    elif command.startswith("ping"):
        ret = ""
	hostname = command[4:]
        if command.startswith("pinga "):
            hostname = command[5:]
        if (hostname.startswith(' o ') or hostname.startswith(' a ')):
	    hostname = hostname[3:]
             
        result = re.search('<http://(.*)\|', hostname) # slack encapsula palavras que tem .com

        if(not result is None):
            hostname = result.string[result.start()+8:result.end()-1]
   
        os.system("sudo ping -c 1 -w 2 " + str(hostname) + " > tmp")
        ret = open('tmp', 'r').read()
        response = "```%s```" % (ret)

        if ret == "":
            response = "@%s não conheço esse cara ae não." % (username)

    for res in sorted(info, key = len):
        if res in command:
            response = info[res][randint(0,len(info[res])-1)]
    file.close()


    slack_client.api_call("chat.postMessage", channel=channel, text=response, as_user=True, attachments = [image])

def parse_slack_output(slack_rtm_output):
    output_list = slack_rtm_output
    if output_list and len(output_list) > 0:
        for output in output_list:

            if output and 'text' in output and AT_BOT in output['text'] or (output.has_key("text") and output.has_key('channel') and BOT_ID in output['channel'] and output['user'] != BOT_ID):
                if (output.has_key("text") and output.has_key('channel') and BOT_ID in output['channel']):
                    output["text"] = "icaro %s" % (output["text"])
                if output["text"][0:6].strip() == "icaro":
                    output["text"] = output["text"][5:]
                return output['text'].strip().lower(), \
                       output['channel'], \
                       output['user']
    return None, None, None 
if __name__ == "__main__":
    READ_WEBSOCKET_DELAY = 1 # 1 second delay between reading from firehose
    if slack_client.rtm_connect():
        print("Bot Rodando!")
        while True:
            command, channel, userid = parse_slack_output(slack_client.rtm_read())
            
            if command and channel:
                handle_command(command, channel, userid)
            time.sleep(READ_WEBSOCKET_DELAY)
    else:
        print("Connection failed. Invalid Slack token or bot ID?")
