import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { JavaLoveComponent } from './java/java-love/java-love.component';
import { JavaSucksComponent } from './java/java-sucks/java-sucks.component';
import { CsLoveComponent } from './csharp/cs-love/cs-love.component';
import { CsNeverSucksComponent } from './csharp/cs-never-sucks/cs-never-sucks.component';

const pageContentRoutes: Routes = [
    { path: 'java-love', component: JavaLoveComponent },
    { path: 'java-sucks', component: JavaSucksComponent },
    { path: 'cs-love', component: CsLoveComponent },
    { path: 'cs-never-sucks', component: CsNeverSucksComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(pageContentRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class PageContentRoutingModule { }
