import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JavaLoveComponent } from './java-love.component';

describe('JavaLoveComponent', () => {
  let component: JavaLoveComponent;
  let fixture: ComponentFixture<JavaLoveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JavaLoveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JavaLoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
