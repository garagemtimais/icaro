import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JavaSucksComponent } from './java-sucks.component';

describe('JavaSucksComponent', () => {
  let component: JavaSucksComponent;
  let fixture: ComponentFixture<JavaSucksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JavaSucksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JavaSucksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
