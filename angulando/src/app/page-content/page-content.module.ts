import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageContentRoutingModule } from './page-content-routing.module';
import { CsLoveComponent } from './csharp/cs-love/cs-love.component';
import { JavaLoveComponent } from './java/java-love/java-love.component';
import { JavaSucksComponent } from './java/java-sucks/java-sucks.component';
import { CsNeverSucksComponent } from './csharp/cs-never-sucks/cs-never-sucks.component';

@NgModule({
    imports: [
        CommonModule,
        PageContentRoutingModule
    ],
    exports: [

    ],
    declarations: [
        CsLoveComponent,
        JavaLoveComponent,
        JavaSucksComponent,
        CsNeverSucksComponent
    ]
})

export class PageContentModule { }
