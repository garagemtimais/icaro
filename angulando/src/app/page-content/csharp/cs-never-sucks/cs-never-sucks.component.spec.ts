import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsNeverSucksComponent } from './cs-never-sucks.component';

describe('CsNeverSucksComponent', () => {
  let component: CsNeverSucksComponent;
  let fixture: ComponentFixture<CsNeverSucksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsNeverSucksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsNeverSucksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
