import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsLoveComponent } from './cs-love.component';

describe('CsLoveComponent', () => {
  let component: CsLoveComponent;
  let fixture: ComponentFixture<CsLoveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsLoveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsLoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
