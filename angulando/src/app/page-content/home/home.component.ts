import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  Fotos = [
    { foto: 'https://img.elo7.com.br/product/zoom/FBCE34/adesivo-paisagem-praia-decorando-com-adesivos.jpg' },
    { foto: 'https://http2.mlstatic.com/painel-paisagem-praia-coqueiros-200x140-multi' +
      '-D_NQ_NP_203525-MLB25461274818_032017-F.jpg' },
    { foto: 'https://http2.mlstatic.com/papel-de-parede-autoadesivo-cachoeira-paisagem-' +
      'natureza-5m-D_NQ_NP_706801-MLB20418926269_092015-F.jpg' },
    { foto: 'https://i.ytimg.com/vi/GyueTJPWBhE/maxresdefault.jpg' }
  ];

  text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam auctor porta metus.' +
    'Duis pulvinar turpis faucibus est varius elementum. Quisque aliquet nibh et nunc facilisis, eget semper velit pretium.' +
    'Sed scelerisque gravida ipsum, vitae tristique arcu tempus nec. Suspendisse ut sem at nibh tempus blandit.' +
    'Sed eleifend nunc eget iaculis mattis. In nec est gravida, porta ipsum vitae, tincidunt elit.' +
    'Ut eget arcu viverra, rhoncus nisi eget, sagittis nulla. Vivamus a odio ante. Integer venenatis aliquet placerat.' +
    'Proin lobortis porta lectus, et interdum sem feugiat vitae. Vivamus vitae massa non dui porttitor tristique nec placerat ante.' +
    'Nullam eleifend urna non ante sollicitudin semper. Aliquam dignissim eleifend erat, a sodales erat.' +
    'Pellentesque mollis mattis enim, ac tincidunt odio vulputate at. Proin pharetra aliquam erat ut aliquam.';
  Result: string;
  url: string;
  contador: number;

  constructor() { }

  ngOnInit() {
    this.contador = 0;
    this.url = this.Fotos[0].foto;
  }

  TrocaTexto() {
    this.text = 'Java Sucks!';
  }

  ResultadoConta() {
    this.Result = 'Acertou Mizeravi!';
  }

  ChangeImage() {
    const tam = this.Fotos.length - 1;

    if (this.contador < tam) {
      this.contador++;
    } else {
      this.contador = 0;
    }
    this.url = this.Fotos[this.contador].foto;
  }
}
