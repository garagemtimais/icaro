import { Component, OnInit, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-menu-top',
  templateUrl: './menu-top.component.html',
  styleUrls: ['./menu-top.component.css']
})

export class MenuTopComponent implements OnInit {
  nomeTela = 'FrameWork 2';
  @Output() navToggle = new EventEmitter();

  _mobileQueryListener: () => any;
  mobileQuery: MediaQueryList;

  navOpen() {
    this.navToggle.emit(true);
  }

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  onDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  ngOnInit() { }

}
