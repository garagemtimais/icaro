import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  menus = [
    {
      name: 'Home',
      route: 'home',
    },
    {
      name: 'Java Menu',
      submenus: [
        {
          name: 'Java is love, Java is life',
          route: '/java-love'
        },
        {
          name: 'Java sucks',
          route: 'java-sucks'
        }
      ]
    },
    {
      name: 'C# Menu',
      submenus: [
        {
          name: 'C# is love, C# is life',
          route: 'cs-love'
        },
        {
          name: 'C# never sucks',
          route: 'cs-never-sucks'
        }
      ]
    }
  ];

  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  onDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  ngOnInit() { }

}
