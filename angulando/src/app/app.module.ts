import { NgModule } from '@angular/core';
import { LayoutModule } from '@angular/cdk/layout';
import { MatListModule } from '@angular/material/list';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatDividerModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { MenuTopComponent } from './page-struct/menu-top/menu-top.component';
import { SidenavComponent } from './page-struct/sidenav/sidenav.component';
import { ProfileComponent } from './page-struct/profile/profile.component';
import { PageContentComponent } from './page-content/page-content.component';
import { FooterComponent } from './page-struct/footer/footer.component';
import { HomeComponent } from './page-content/home/home.component';
import { PageContentModule } from './page-content/page-content.module';

@NgModule({
  declarations: [
    AppComponent,
    MenuTopComponent,
    SidenavComponent,
    ProfileComponent,
    PageContentComponent,
    FooterComponent,
    HomeComponent,
  ],
  imports: [
    AppRoutingModule,
    PageContentModule,
    BrowserModule,
    MatMenuModule,
    MatCardModule,
    MatIconModule,
    MatListModule,
    MatButtonModule,
    MatToolbarModule,
    MatSidenavModule,
    MatGridListModule,
    MatExpansionModule,
    MatButtonToggleModule,
    BrowserAnimationsModule,
    MatGridListModule,
    LayoutModule,
    MatInputModule,
    MatDividerModule,
    FlexLayoutModule
  ],
  providers: [

  ],
  bootstrap: [
    AppComponent
  ],
})

export class AppModule { }
